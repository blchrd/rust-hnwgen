# HNW Generator

## Foreword

This repository is a rewriting of the first Java version in Rust.

The Java version is still available [here](https://framagit.org/blchrd/hnwgen).

## Description

Generate a [Harsh Noise Wall](https://en.wikipedia.org/wiki/Harsh_noise_wall)
album and its artwork cover.

In short, here what is happening when generating a track :

1. Generate noise source
2. Apply some EQ and / or pitch change with FFmpeg
3. Do 1 and 2 multiple time (one for each layer of noise)
4. Mix all the layers

You can of course release the output like one of your album, if you do so, please include a link to this page in the album credits.

## Getting started

For now, there isn't any release pipeline, so you have to build the executable from the source code.

To do that, you'll need to install the Rust toolchains by following the instruction on the [official website](https://www.rust-lang.org/tools/install).

### Dependencies

This project requires the following dependencies:

* FFmpeg
* SoX (optional)

#### Windows Environment

To run this project on a Windows environment, make sure FFmpeg is installed and in your PATH.

1. Download and install FFmpeg from the official website: [FFmpeg](https://ffmpeg.org/download.html)
2. Download and install SoX: [https://sourceforge.net/projects/sox/](https://sourceforge.net/projects/sox/)

After installing both dependencies, here's how you can add them to the PATH:

1. Open the Start menu and search for "Environment Variables" and select "Edit the system environment variables".
2. In the System Properties window, click on the "Environment Variables" button.
3. In the "System Variables" section, select the "Path" variable and click on the "Edit" button.
4. In the "Edit Environment Variable" window, click on the "New" button.
5. Enter the path to the FFmpeg installation directory (e.g., `C:\path\to\ffmpeg`) and click "OK".
7. Click "OK" to close all the windows.

#### Linux Environment

If you are using a Linux environment, you can install the dependencies directly without modifying the PATH.

Open the terminal and execute the following command:

```bash
sudo apt-get install ffmpeg sox
```

### Build the executable

Once all the tools and dependencies are installed, you can build the executable with the following command.

```bash
cargo build --release
```

The executable is generated in `target/release`.

### CLI usage

No GUI is planned right now, but if you want to build one because CLI isn't your thing, please do!

```bash
cargo run -- -h
```

You can generate a debug release with the following command, it will generate 6 tracks of 10 seconds each.

```bash
cargo run -- --debug
```

## Authors and acknowledgment

Special thanks to JLIAT for authorizing me to use its [noise generator](http://www.jliat.com/HNW/index.html).

## Licence

This work is licenced under GPLv3 licence.