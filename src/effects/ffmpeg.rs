use std::process::Command;

pub fn convert_file(infile: &str, outfile: &str) {
    let mut binding = Command::new("ffmpeg");
    let ffmpeg_command = binding.args([
        "-y",
        "-i",
        &infile,
        &outfile,
    ]);
    let _ = ffmpeg_command.output();
}

pub fn apply_eq(infile: &str, outfile: &str, frequency: i32, width: i32, gain: i32) {
    let mut binding = Command::new("ffmpeg");
    let ffmpeg_command = binding.args([
        "-y",
        "-i",
        &infile,
        "-af",
        &(format!("equalizer=f={}:width_type=h:width={}:g={}", frequency.to_string(), width.to_string(), gain.to_string())),
        &outfile,
    ]);
    let _ = ffmpeg_command.output();
}

pub fn apply_pitch(infile: &str, outfile: &str, rate: f32) {
    let mut binding = Command::new("ffmpeg");
    let ffmpeg_command = binding.args([
        "-y",
        "-i",
        &infile,
        "-af",
        &format!("asetrate=44100*{},aresample=44100,atempo=1/{}", rate.to_string(), rate.to_string()),
        &outfile,
    ]);
    let _ = ffmpeg_command.output();
}

pub fn cut_volume(infile: &str, outfile: &str, start_time: i32, end_time: i32) {
    let mut binding = Command::new("ffmpeg");
    let ffmpeg_command = binding.args([
        "-y",
        "-i",
        &infile,
        "-af",
        &format!("volume=enable='between(t,{},{})':volume=0", start_time.to_string(), end_time.to_string()),
        &outfile,
    ]);
    let _ = ffmpeg_command.output();
}

pub fn mix_layers(layers: Vec<String>, outfile: &str) {
    if layers.len() > 1 {
        let mut binding = Command::new("ffmpeg");
        for layer in layers.clone() {
            binding.args(["-i", &layer]);
        }
        let ffmpeg_command = binding.args([
            "-filter_complex",
            &format!("amix=inputs={}:duration=longest", layers.len()),
            &outfile
        ]);
        let _ = ffmpeg_command.output();
    } else {
        // if we have one layer, we just rename the file
        if let Err(e) = std::fs::rename(&layers[0], outfile) {
            eprintln!("Error renaming file: {}", e);
        }
    }
}

pub fn generate_silent_track(outfile: &str, duration_in_seconds: i32) {
    let mut binding = Command::new("ffmpeg");
    let ffmpeg_command = binding.args([
        "-f",
        "lavfi",
        "-i",
        "anullsrc=r=22050:cl=mono",
        "-t",
        &(duration_in_seconds.to_string()),
        "-ar",
        "44100",
        "-y",
        &outfile,
    ]);
    let _ = ffmpeg_command.output();
}

pub fn resample(infile: &str, outfile: &str) {
    let mut binding = Command::new("ffmpeg");
    let ffmpeg_command = binding.args([
        "-y",
        "-i",
        &infile,
        "-ar",
        "44100",
        &outfile,
    ]);
    let _ = ffmpeg_command.output();
}

pub fn cut(infile: &str, outfile: &str, from: i32, time: i32) {
    let mut binding = Command::new("ffmpeg");
    let ffmpeg_command = binding.args([
        "-y",
        "-ss",
        &(from.to_string()),
        "-i",
        &infile,
        "-t",
        &(time.to_string()),
        &outfile,
    ]);
    let _ = ffmpeg_command.output();
}

pub fn fade_in(infile: &str, outfile: &str, time_in_second: i32, start_time: i32) {
    let mut binding = Command::new("ffmpeg");
    let ffmpeg_command = binding.args([
        "-y",
        "-i",
        &infile,
        "-af",
        &format!("afade=t=in:st={}:d={}", start_time.to_string(), time_in_second.to_string()),
        &outfile,
    ]);
    let _ = ffmpeg_command.output();
}

pub fn fade_out(infile: &str, outfile: &str, time_in_second: i32, start_time: i32) {
    let mut binding = Command::new("ffmpeg");
    let ffmpeg_command = binding.args([
        "-y",
        "-i",
        &infile,
        "-af",
        &format!("afade=t=out:st={}:d={}", start_time.to_string(), time_in_second.to_string()),
        &outfile,
    ]);
    let _ = ffmpeg_command.output();
}

pub fn finalize(infile: &str, outfile: &str, volume: i32) {
    let mut binding = Command::new("ffmpeg");
    let ffmpeg_command = binding.args([
        "-y",
        "-i",
        &infile,
        "-filter:a",
        &(format!("volume={}", volume.to_string())),
        &outfile,
    ]);
    let _ = ffmpeg_command.output();
}