use std::process::Command;

use rand::{rngs::SmallRng, Rng};

#[derive(Copy, Clone)]
pub enum Effect {
    Overdrive,
    Bass,
    Treble,
    Pitch,
    Reverb,
    Hilbert,
    Stretch,
    Equalizer,
    Mcompand,
    Lowpass,
    Highpass,
    Sinc,
    Gain,
}

impl Effect {
    fn random_effect(random: &mut SmallRng) -> Self {
        use Effect::*;
        let variants = [
            Overdrive, Bass, Treble, Pitch, Reverb, Hilbert, Stretch, Equalizer, Mcompand, Lowpass,
            Highpass, Sinc, Gain,
        ];
        let index = random.gen_range(0..variants.len());
        variants[index]
    }
}

pub fn apply_random_sox_effects(infile: &str, outfile: &str, nb_effects: i32, random: &mut SmallRng) {
    let mut binding = Command::new("sox");

    binding.args([
        infile,
        outfile
    ]);

    let mut nb_applied_effects = 0;
    let mut cumulative_pitch = 0;
    let mut highpass_nb = 0;
    let mut lowpass_nb = 0;
    let mut treble_nb = 0;
    let mut mcompand_nb = 0;
    
    while nb_applied_effects < nb_effects {
        let effect = Effect::random_effect(random);
        let mut effect_ok = false;

        match effect {
            Effect::Mcompand => {
                mcompand_nb += 1;
                if mcompand_nb > 1 {
                    binding.args([
                        "mcompand",
                        "0.000625,0.0125 -47,-40,-34,-34,-15,-33",
                        "1600",
                        "0.0001,0.025 -47,-40,-34,-34,-31,-31,-0,-30",
                        "6400",
                        "0,0.025 -38,-31,-28,-28,-0,-25",
                    ]);
                    //mcompand "0.000625,0.0125 −47,−40,−34,−34,−15,−33" 1600 "0.0001,0.025 −47,−40,−34,−34,−31,−31,−0,−30" 6400 "0,0.025 −38,−31,−28,−28,−0,−25" gain 15 highpass 22 highpass 22 sinc −n 255 −b 16 −17500 gain 9 lowpass −1 17801
                    effect_ok = true;
                }
            },
            Effect::Bass => {
                binding.args(["bass", &random.gen_range(0..21).to_string()]);
                effect_ok = true;
            },
            Effect::Treble => {
                treble_nb += 1;
                if treble_nb > 1 {
                    binding.args(["treble", &random.gen_range(0..=5).to_string()]);
                    effect_ok = true;
                }
            },
            Effect::Overdrive => {
                binding.args(["overdrive", &random.gen_range(0..21).to_string()]);
                effect_ok = true;
            },
            Effect::Pitch => {
                let pitch_rate = random.gen_range(0..10) * 100 * -1;
                cumulative_pitch += pitch_rate;
                if cumulative_pitch > -1500 {
                    binding.args(["pitch", &cumulative_pitch.to_string()]);
                    effect_ok = true;
                }
            },
            Effect::Lowpass => {
                lowpass_nb += 1;
                if lowpass_nb > 1 {
                    binding.args(["lowpass", &random.gen_range(1..=11).to_string()]);
                    effect_ok = true;
                }
            },
            Effect::Highpass => {
                highpass_nb += 1;
                if highpass_nb > 2 {
                    binding.args(["highpass", &random.gen_range(1..=22).to_string()]);
                    effect_ok = true;
                }
            },
            Effect::Reverb => {
                binding.args(["reverb"]);
                effect_ok = true;
            },
            Effect::Hilbert => {},
            Effect::Stretch => {},
            Effect::Equalizer => {},
            Effect::Sinc => {},
            Effect::Gain => {},
        }

        if effect_ok {
            nb_applied_effects += 1;
        }
    }

    let _ = binding.status();
}