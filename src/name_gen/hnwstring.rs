use crate::Config;
use regex::Regex;
use rand::{rngs::SmallRng, Rng};

use super::namegenerator;

pub enum Type {
    ProjectName,
    AlbumTitle,
    TrackTitle,
    CoverTitle,
}

#[derive(Debug, Clone, Default)]
pub struct GeneratedStrings {
    pub project_name: Option<String>,
    pub album_title: Option<String>,
}

fn build_string_from_template(template: String, generated_strings: GeneratedStrings, track_number: u32, config: &Config, random: &mut SmallRng) -> String {
    let mut built_string = template;

    let rand_int = random.gen_range(0..=536862719) as i64;
    let rand_hex = format!("{:08x}", rand_int);

    built_string = built_string.clone().replace("%%RANDHEX%%", &rand_hex);

    built_string = built_string.clone().replace("%%PROJECTNAME%%", &generated_strings.project_name.unwrap_or("".to_string()));
    built_string = built_string.clone().replace("%%ALBUMTITLE%%", &generated_strings.album_title.unwrap_or("".to_string()));

    if built_string.clone().contains("%%GLOBALCOUNTER1%%") {
        built_string = built_string.clone().replace("%%GLOBALCOUNTER1%%", &config.global_counter_1.to_string());
        config.clone().increment_global_counter_1();
    }

    if built_string.clone().contains("%%GLOBALCOUNTER2%%") {
        built_string = built_string.clone().replace("%%GLOBALCOUNTER2%%", &config.global_counter_2.to_string());
        config.clone().increment_global_counter_2();
    }

    if built_string.clone().contains("%%TRACKNUMBER%%") {
        built_string = built_string.clone().replace("%%TRACKNUMBER%%", &track_number.to_string());
    }

    let namegen_regex = Regex::new(r"%%NAMEGEN\[(?<template>.*)\]%%").unwrap();
    let caps = namegen_regex.captures(&built_string);
    if caps.is_some() {
        let template = caps.unwrap()["template"].to_string();
        let replace = format!("%%NAMEGEN[{}]%%", template);
        built_string = built_string.clone().replace(&replace, &namegenerator::generate_name(template));
    }

    built_string
}


pub fn get_string(type_string: Type, generated_strings: GeneratedStrings, track_number: u32, random: &mut SmallRng, config: &Config) -> Option<String> {
    let mut string: Option<String>;

    match type_string {
        Type::ProjectName => {
            string = Some(build_string_from_template(config.clone().project_name.to_string(), generated_strings, track_number, config, random));
        },
        Type::AlbumTitle => {
            string = Some(build_string_from_template(config.clone().album_title_template.to_string(), generated_strings, track_number, config, random));
        },
        Type::TrackTitle => {
            string = Some(build_string_from_template(config.clone().track_title_template, generated_strings, track_number, config, random));
        },
        Type::CoverTitle => {
            string = Some(build_string_from_template(config.clone().cover_album_title_template, generated_strings, track_number, config, random));
        }
    }

    if string.clone().unwrap_or("".to_string()) == "".to_string() {
        string = None;
    }

    string
}

