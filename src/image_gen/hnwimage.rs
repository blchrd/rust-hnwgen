extern crate colors_transform;
extern crate image;

use colors_transform::{Color, Rgb as RgbTransform};
use image::{Rgb, RgbImage, ImageBuffer, Pixel, imageops::FilterType, GenericImageView};
use imageproc::drawing::{draw_text, draw_text_mut};
use rand::{rngs::SmallRng, Rng};
use rusttype::{Font, Scale};

use super::lsystem;

pub struct HnwImage {
    width: u32,
    height: u32,
    font_title: Font<'static>,
    font_background: Font<'static>,
    lsystem: lsystem::LSystem,
}

impl HnwImage {
    pub fn new(width: u32, height: u32) -> Self {
        let font_data: &[u8] = include_bytes!("../../fonts/DejaVuSansMono.ttf");
        let font_title: Font<'static> = Font::try_from_bytes(font_data).unwrap();
        let font_background: Font<'static> = Font::try_from_bytes(font_data).unwrap();
        Self {
            width,
            height,
            font_title,
            font_background,
            lsystem: lsystem::LSystem::default(),
        }
    }

    pub fn generate_tribute_cover(&mut self, infile: &str, outfile: &str, title: &str, mut random: &mut SmallRng) {
        let source_img = image::open(infile).expect("Failed to open image file");
        let mut img = RgbImage::new(source_img.width(), source_img.height());

        for (x, y, pixel) in source_img.to_rgba8().enumerate_pixels() {
            img.put_pixel(x, y, Rgb([pixel[0], pixel[1], pixel[2]]));
        }

        for i in 0..self.width {
            for j in 0..self.height {
                if random.gen_range(0..=4) == 2 {
                    let grey = random.gen_range(0..=100);
                    let color = Rgb([grey as u8, grey as u8, grey as u8]);
                    img.put_pixel(i, j, color);
                }
            }
        }

        let image_with_title = self.add_title_on_image(img, title, &mut random);
        let image_final = self.add_background_hexa(image_with_title, &mut random);

        image_final.save(outfile).expect("Failed to save image");
    }

    pub fn generate_image(&mut self, outfile: &str, title: &str, taint_image: bool, l_system: bool, mut random: &mut SmallRng) {
        let mut img = RgbImage::new(self.width, self.height);

        for i in 0..self.width {
            for j in 0..self.height {
                let r = random.gen_range(0..=100) as u8;
                let g = random.gen_range(0..=100) as u8;
                let b = random.gen_range(0..=100) as u8;
                let color = Rgb([r, g, b]);
                img.put_pixel(i, j, color);
            }
        }

        if taint_image {
            img = self.tainting_image(img, &mut random);
        }

        let image_with_hexa = self.add_background_hexa(img, &mut random);

        let image_final: ImageBuffer<Rgb<u8>, Vec<u8>>;
        if l_system {
            let image_with_lsystem = self.add_l_system_render(image_with_hexa.clone(), &mut random);
            image_final = self.add_title_on_image(image_with_lsystem, title, &mut random);
        } else {
            image_final = self.add_title_on_image(image_with_hexa, title, &mut random);
        }

        image_final.save(outfile).expect("Failed to save image");
    }

    pub fn get_lsystem_json(&self) -> String {
        self.lsystem.get_json()
    }

    fn add_background_hexa(&mut self, image: RgbImage, random: &mut SmallRng) -> ImageBuffer<Rgb<u8>, Vec<u8>> {
        let mut image_temp = image.clone();
        for j in 0..100 {
            for i in 0..24 {
                let background_text = format!("{:x}", random.gen_range(0..=536862719));
                let (x, y) = (i * 65, j * 17);
                draw_text_mut(
                    &mut image_temp,
                    Rgb([100,100,100]),
                    x as i32,
                    y as i32,
                    Scale{x: 15.0, y: 15.0},
                    &self.font_background,
                    &background_text,
                );
            }
        }

        image_temp
    }

    fn add_title_on_image(&mut self, image: RgbImage, title: &str, random: &mut SmallRng) -> ImageBuffer<Rgb<u8>, Vec<u8>> {
        let x = random.gen_range(50..=350) as i32;
        let y = random.gen_range(150..=1000) as i32;
        let mut returned_image: ImageBuffer<Rgb<u8>, Vec<u8>> = draw_text(&image, Rgb([196,196,196]), x, y, Scale{x: 60.0, y: 60.0}, &self.font_title, title);

        let parts = title.split("\\r\\n");
        println!("{}", parts.clone().count());
        if parts.clone().count() > 1 {
            let mut y_offset = 0;
            returned_image = image.clone();
            for part in parts {
                returned_image = draw_text(&returned_image, Rgb([196,196,196]), x, y+y_offset, Scale{x: 60.0, y: 60.0}, &self.font_title, part);
                y_offset += 70;
            }
        }

        returned_image
    }

    fn tainting_image(&mut self, source_image: RgbImage, random: &mut SmallRng) -> RgbImage {
        let width = source_image.width();
        let height = source_image.height();
        let mut tainted_image = RgbImage::new(width, height);

        let input_hue = random.gen_range(0..360) as f32;

        for (x, y, pixel) in source_image.enumerate_pixels() {
            let r = pixel[0] as f32;
            let g = pixel[1] as f32;
            let b = pixel[2] as f32;

            let rgb = RgbTransform::from(r, g, b);
            let modified = rgb.set_hue(input_hue);

            tainted_image.put_pixel(
                x,
                y,
                Rgb([
                    modified.get_red() as u8,
                    modified.get_green() as u8,
                    modified.get_blue() as u8,
                ]),
            );
        }

        tainted_image
    }

    fn add_l_system_render(&mut self, source_image: RgbImage, mut random: &mut SmallRng) -> RgbImage {
        self.lsystem.build_random_lsystem(&mut random);
        let mut l_system_render = self.lsystem.rendering_lsystem();

        // Maximum width
        if l_system_render.width() > 1400 {
            println!("L-System: width sizing down");
            let new_width: u32 = 1400;
            let new_height: u32 = (l_system_render.height() as f32 / (l_system_render.width() as f32 / new_width as f32)) as u32;
            l_system_render = image::imageops::resize(&l_system_render, new_width, new_height, FilterType::Nearest);
        }

        // Maximum height
        if l_system_render.width() > 1400 {
            println!("L-System: height sizing down");
            let new_height: u32 = 1400;
            let new_width: u32 = (l_system_render.width() as f32 / (l_system_render.height() as f32 / new_height as f32)) as u32;
            l_system_render = image::imageops::resize(&l_system_render, new_width, new_height, FilterType::Nearest);
        }

        // Minimum width
        if l_system_render.width() < 400 {
            println!("L-System: sizing up");
            let new_width: u32 = 400;
            let new_height: u32 = (l_system_render.height() as f32 / (l_system_render.width() as f32 / new_width as f32)) as u32;
            l_system_render = image::imageops::resize(&l_system_render, new_width, new_height, FilterType::Nearest);
        }

        //We change the position of the image (but keep it into the 1400 x 1400 boundaries)
        let mut translate_x = 0;
        let mut translate_y = 0;
        if l_system_render.width() < 1400 || l_system_render.height() < 1400 {
            translate_x = random.gen_range(0..=1400 - l_system_render.width());
            if l_system_render.height() < 1400 {
                translate_y = random.gen_range(0..=1400 - l_system_render.height());
            }
            println!("L-System: x+{} / y+{}", translate_x, translate_y)
        }

        // Merge the two image
        let mut new_image = source_image.clone();
        for (x, y, pixel) in l_system_render.enumerate_pixels() {
            let new_x = x + translate_x;
            let new_y = y + translate_y;
            if new_image.in_bounds(new_x, new_y) {
                if pixel.channels()[3] > 0 {
                    new_image.put_pixel(new_x, new_y, Rgb([pixel.channels()[0],pixel.channels()[1],pixel.channels()[2]]));
                } 
            }
        }

        new_image
    }
}
