mod noise_gen;
mod name_gen;
mod image_gen;
mod effects;

use clap::Parser;
use confy::{self};
use noise_gen::{hnwalbum::HnwAlbum, hnwwithsource::HnwWithSource};
use image_gen::hnwimage::HnwImage;
use serde::{Deserialize, Serialize};
use rand::{rngs::SmallRng, RngCore, SeedableRng};
use std::{fs, io::stdin};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Config {
    project_name: String,
    album_title_template: String,
    cover_album_title_template: String,
    track_title_template: String,
    global_counter_1: u32,
    global_counter_2: u32,
}

impl ::std::default::Default for Config {
    fn default() -> Self {
        Self {
            project_name: "Unknown".into(),
            album_title_template: "Untitled".into(),
            cover_album_title_template: "Untitled".into(),
            track_title_template: "Untitled %%GLOBALCOUNTER1%%".into(),
            global_counter_1: 1,
            global_counter_2: 1,
        }
    }
}

impl Config {
    fn increment_global_counter_1(&mut self) {
        self.global_counter_1 += 1;
        _ = confy::store("hnw", None, self);
    }

    fn increment_global_counter_2(&mut self) {
        self.global_counter_2 += 1;
        _ = confy::store("hnw", None, self);
    }
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Execute configuration utility
    #[arg(short, long, default_value_t = false)]
    config: bool,

    /// Length of album in minutes
    #[arg(short, long, default_value_t = 20)]
    length: i32,

    /// Number of tracks in album
    #[arg(short='n', long, default_value_t = 2)]
    nb_tracks: i32,

    /// Number of layers for each track
    #[arg(short='y', long, default_value_t = 3)]
    nb_layers: i32,

    /// Enable pitch change in layer generation
    #[arg(short, long, default_value_t = false)]
    pitch: bool,

    /// Enable wall variation in layer generation
    #[arg(short, long, default_value_t = false)]
    variation: bool,

    /// Enable the tainting of generated cover
    #[arg(short='C', long, default_value_t = false)]
    taint_cover: bool,

    /// Enable L-System rendering in the artwork
    #[arg(short='L', long, default_value_t = false)]
    l_system: bool,

    /// Apply effect after the layer mix (only sox effect for now)
    #[arg(short='P', long, default_value_t = false)]
    post_effect: bool,

    /// Only generate the cover, you can force the title with the --tribute-title argument
    #[arg(short='i', long, default_value_t = false)]
    only_cover: bool,

    /// Disable FFMPEG effects
    #[arg(long, default_value_t = false)]
    noffmpeg: bool,

    /// Enable SoX effects (experimental)
    #[arg(long, default_value_t = false)]
    sox: bool,

    /// Start generation of a debug release, 6 tracks of 10 seconds each
    #[arg(long, default_value_t = false)]
    debug: bool,

    /// Seed for random generation
    #[arg(long, default_value_t = 0)]
    seed: u64,

    /// Audio source for noise (work in progress), you must pass an output file with this parameters
    #[arg(short, long, default_value = "")]
    source_file: Box<str>,

    /// Output destination file (work in progress)
    #[arg(short, long, default_value = "")]
    output_file: Box<str>,

    /// Generate a tribute album with all wav file in path, the track will have 10 minutes length
    #[arg(short='r', long, default_value = "")]
    tribute: Box<str>,

    /// Enable the generation of cover for tribute album. Need a file named "cover.jpg" in tribute directory
    #[arg(short='w', long, default_value_t = false)]
    with_tribute_cover: bool,

    /// Only generate the tribute cover, based on the parameters
    #[arg(short='I', long, default_value = "")]
    only_tribute_cover: Box<str>,

    /// Title of the tribute album, for the cover generation
    #[arg(short='T', long, default_value = "")]
    tribute_title: Box<str>,
}

fn main() {
    let mut args = Args::parse();
    let config: Config = confy::load("hnw", None).ok().unwrap_or_default();

    if args.config {
        run_configuration(config);
        return;
    }

    let seed: u64;
    if args.seed == 0 {
        let mut seed_gen = SmallRng::from_entropy();
        seed = seed_gen.next_u64();
    } else {
        seed = args.seed;
    }

    println!("Seed: {}", seed);
    let mut random = SmallRng::seed_from_u64(seed);

    // Checking incorrect parameters
    if args.tribute.to_string() != "" {
        let mut metadata = fs::metadata(args.tribute.to_string()).ok();
        if metadata.is_some() {
            if !metadata.unwrap().is_dir() {
                panic!("Tribute path is not a directory")
            }
        } else {
            panic!("Tribute path does not exists")
        }

        if args.with_tribute_cover {
            metadata = fs::metadata(format!("{}/cover.jpg", args.tribute.to_string())).ok();
            if !metadata.is_some() {
                println!("No base cover for tribute cover generation. Generate the album without the cover");
                args.with_tribute_cover = false;
            }
        }
    } else if args.source_file.to_string() != "" {
        let metadata = fs::metadata(args.source_file.to_string()).ok();
        if !metadata.is_some() {
            panic!("Source file does not exists")
        } else if args.output_file.to_string() == "" {
            panic!("Output file absent from command line parameters")
        }
    }

    // Apply effect to input file
    if args.source_file.to_string() != "" {
        println!("Apply effects for {} to {}", args.source_file.to_string(), args.output_file.to_string());
        let hnw_with_source = HnwWithSource{
            pitch: args.pitch,
            ffmpeg_enabled: !args.noffmpeg,
            sox_enabled: args.sox
        };

        println!("{} --> {}", args.source_file.to_string(), &args.output_file.to_string());
        hnw_with_source.apply_effect_to_source(&args.source_file.to_string(), &args.output_file.to_string(), &mut random);
        println!("Effects application done");
        return;
    }

    // Only generate the cover
    if args.only_cover {
        let mut cover_image = HnwImage::new(1400, 1400);
        let mut title = args.tribute_title.to_string();
        if title == "".to_string() {
            title = "unknown - untitled".to_string();
        }
        cover_image.generate_image(
            "./test-cover.jpg",
            &title,
            args.taint_cover,
            args.l_system,
            &mut random
        );

        println!("{}", "Cover generation done");
        return;
    }

    // Only generate the tribute cover
    if args.only_tribute_cover.to_string() != "" {
        let metadata = fs::metadata(args.tribute.to_string()).ok();
        if metadata.is_some() {
            if !metadata.unwrap().is_file() {
                panic!("Base cover path is not a file")
            }
        } else {
            panic!("Base cover file does not exist")
        }

        let mut hnw_image = HnwImage::new(1400, 1400);
        hnw_image.generate_tribute_cover(
            &args.only_tribute_cover.to_string(),
            "./test-tribute-cover.jpg",
            &args.tribute_title.to_string(),
            &mut random
        );

        println!("{}", "Tribute cover generation done");
        return;
    }

    if args.debug {
        args.length = 1;
        args.nb_tracks = 6;
    }

    let mut hnw_album = HnwAlbum::new(
        args.length,
        args.nb_tracks,
        args.nb_layers,
        args.pitch,
        args.variation,
        args.taint_cover,
        args.l_system,
        args.tribute.to_string(),
        args.with_tribute_cover,
        args.tribute_title.to_string(),
        !args.noffmpeg,
        args.sox,
        args.post_effect,
        seed,
        config,
    );

    hnw_album.generate_hnw_album(&mut random);

    println!("Generation done!");
}

fn run_configuration(mut conf: Config) {
    println!("[1] Project name = \"{}\"", conf.project_name);
    println!("[2] Album title template = \"{}\"", conf.album_title_template);
    println!("[3] Track title template = \"{}\"", conf.track_title_template);
    println!("[4] Cover album title template = \"{}\"", conf.cover_album_title_template);
    println!("[5] Reset global counter 1 (current: {})", conf.global_counter_1);
    println!("[6] Reset global counter 2 (current: {})", conf.global_counter_2);
    let mut exit = false;

    while !exit {
        exit = false;
        println!("\nEnter the number of the setting to change, h for template help, n for namegen template help, or q to quit");

        let mut choice = String::new();
        _ = stdin().read_line(&mut choice);
        choice = choice.trim_end().to_string();

        let mut input = String::new();
        if choice == "1" {
            println!("Enter new project name (empty to keep the current value):");
            _ = stdin().read_line(&mut input);
            if input != "".to_string() {
                input = input.trim_end().to_string();
                conf.project_name = input;
                _ = confy::store("hnw", None, conf.clone());
                println!("Project name changed successfully");
            }
        } else if choice == "2" {
            println!("Enter new album title template (empty to keep the current value):");
            _ = stdin().read_line(&mut input);
            if input != "" {
                input = input.trim_end().to_string();
                conf.album_title_template = input;
                _ = confy::store("hnw", None, conf.clone());
                println!("Album title template changed successfully");    
            }
        } else if choice == "3" {
            println!("Enter new track title template (empty to keep the current value):");
            _ = stdin().read_line(&mut input);
            if input != "" {
                input = input.trim_end().to_string();
                conf.track_title_template = input;
                _ = confy::store("hnw", None, conf.clone());
                println!("Track title template changed successfully");    
            }
        } else if choice == "4" {
            println!("Enter new cover album title template (empty to keep the current value):");
            _ = stdin().read_line(&mut input);
            if input != "" {
                input = input.trim_end().to_string();
                conf.cover_album_title_template = input;
                _ = confy::store("hnw", None, conf.clone());
                println!("Cover album title template changed successfully");    
            }
        } else if choice == "5" {
            conf.global_counter_1 = 1;
            _ = confy::store("hnw", None, conf.clone());
            println!("Success! Global counter 1 reset to 1")
        } else if choice == "6" {
            conf.global_counter_2 = 1;
            _ = confy::store("hnw", None, conf.clone());
            println!("Success! Global counter 2 reset to 1")
        } else if choice == "h" {
            println!("|-------------------------|-------------------------------------------------|");
            println!("| Template                | Replacing string                                |");
            println!("|-------------------------|-------------------------------------------------|");
            println!("| %%PROJECTNAME%%         | Name of the project                             |");
            println!("| %%ALBUMTITLE%%          | Title of the album                              |");
            println!("| %%RANDHEX%%             | Random hexadecimal number                       |");
            println!("| %%GLOBALCOUNTER1%%      | Global counter 1                                |");
            println!("| %%GLOBALCOUNTER2%%      | Global counter 2                                |");
            println!("| %%TRACKNUMBER%%         | Track number (0 if outside of a track template) |");
            println!("| %%NAMEGEN[<template>]%% | Use name generator (empty template for random)  |");
            println!("|-------------------------|-------------------------------------------------|");
        } else if choice == "n" {
            println!("|-------------|-------------------------------------------------------------------------|");
            println!("|  Character  | Meaning                                                                 |");
            println!("|-------------|-------------------------------------------------------------------------|");
            println!("|      s      | Generic syllable                                                        |");
            println!("|      v      | Single vowel                                                            |");
            println!("|      d      | Single diacritic vowel                                                  |");
            println!("|      V      | Single vowel or vowel combination                                       |");
            println!("|      c      | Single consonant                                                        |");
            println!("|      D      | Single diacritic consonant                                              |");
            println!("|      B      | Single consonant or consonant combination suitable for beginning a word |");
            println!("|      C      | Single consonant or consonant combination suitable anywhere in a word   |");
            println!("|      '      | Literal apostrophe                                                      |");
            println!("|      -      | Literal hyphen                                                          |");
            println!("|     ( )     | Denotes that anything inside is literal                                 |");
            println!("|     < >     | Denotes that anything inside is a special symbol                        |");
            println!("|      |      | Logical 'OR' operator for use in ( ) and < >                            |");
            println!("|-------------|-------------------------------------------------------------------------|");
        } else if choice == "q" || choice == "exit" || choice == "quit" || choice == "" {
            exit = true;
        } else {
            println!("Invalid input");
        }
    }
}