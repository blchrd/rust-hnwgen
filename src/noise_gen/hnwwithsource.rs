use std::fs;

use rand::rngs::SmallRng;
use super::hnwlayer;

pub struct HnwWithSource {
    pub pitch: bool,
    pub ffmpeg_enabled: bool,
    pub sox_enabled: bool,
}

impl HnwWithSource {
    pub fn apply_effect_to_source(&self, infile: &str, outfile: &str, mut random: &mut SmallRng) {
        let work_path: String = "generation/tmp".to_string();
        let metadata = fs::metadata(work_path.clone()).ok();
        if metadata.is_none() {
            fs::create_dir_all(work_path.clone()).ok();
        }

        let mut layer = hnwlayer::HnwLayer::new(
            outfile.to_string(),
            work_path.clone(),
            0,
            0,
            self.pitch,
            false,
        );
        layer.ffmpeg_enabled = self.ffmpeg_enabled;
        layer.sox_enabled = self.sox_enabled;
        layer.generate(&mut random, Some(infile.to_string()));

        //clean the working directory
        fs::remove_dir_all(work_path.clone()).ok();
    }
}