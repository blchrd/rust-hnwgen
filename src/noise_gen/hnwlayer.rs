use crate::effects::ffmpeg;
use crate::effects::sox;
use super::hnwfile::HnwFile;
use super::hnweq::HnwEQ;
use super::hnweq::Freq;
use rand::rngs::SmallRng;
use rand::Rng;

pub struct HnwLayer {
    pub outfile: String,
    length: i32,
    pub path: String,
    loudness: i16,
    eq_details: String,
    pitch_chance: bool,
    pitch_rate: f32,
    variation_chance: bool,
    pub ffmpeg_enabled: bool,
    pub sox_enabled: bool,
}

impl HnwLayer {
    pub fn new(outfile: String, path: String, length: i32, loudness: i16, pitch_chance: bool, variation_chance: bool) -> Self {
        Self{
            outfile,
            length,
            path,
            loudness,
            eq_details: "".to_string(),
            pitch_chance,
            pitch_rate: 0.0,
            variation_chance,
            ffmpeg_enabled: true,
            sox_enabled: false,
        }
    }

    pub fn generate(&mut self, mut random: &mut SmallRng, input_file: Option<String>) {
        // Files path
        let output_file_au = format!("{}/{}.AU", self.path, self.outfile);
        let output_file_wav;
        if input_file.is_none() {
            output_file_wav = format!("{}/{}", self.path, self.outfile);
        } else {
            output_file_wav = self.outfile.clone();
        }
        let temp_file_1 = format!("{}/temp.wav", self.path);
        let temp_file_2 = format!("{}/final.wav", self.path);
        let mut temp_file_final = format!("{}/final2.wav", self.path);
        let mut temp_file_final2 = format!("{}/final3.wav", self.path);

        // Hnw parameters
        let harshness: usize = random.gen_range(80..=2580);
        let mut variation: bool = false;

        if self.variation_chance {
            variation = random.gen_bool(0.2);
        }

        if input_file.is_none() {
            println!("Harshness: {}", harshness.to_string());
            println!("Variation: {}", variation.to_string());
        }

        if input_file.is_none() {
            //Call HNW Magick maker
            let mut hnw_file = HnwFile::new(harshness, self.length, self.loudness, variation);
            hnw_file.generate_audio(&output_file_au, &mut random.clone());
            //Convert .AU --> .wav
            ffmpeg::convert_file(&output_file_au, &temp_file_1);
        } else {
            //Convert input file
            ffmpeg::convert_file(&input_file.unwrap(), &temp_file_1);
        }

        //Apply ffmpeg if enabled
        if self.ffmpeg_enabled {
            // EQ
            self.apply_eq(&temp_file_1, &temp_file_2, &mut random);

            // Resample
            ffmpeg::resample(&temp_file_2, &temp_file_final);

            // Pitch
            self.pitch_rate = 1.0;
            if self.pitch_chance {
                if random.gen_bool(0.5) {
                    let temp_file_pitched = format!("{}/final_pitched.wav", self.path);
                    let random_float = random.gen::<f32>();
                    let scaled_float = 0.2 + random_float * (1.5 - 0.2);
                    self.pitch_rate = scaled_float;
                    println!("Pitch: {}", self.pitch_rate.to_string());
                    ffmpeg::apply_pitch(&temp_file_final, &temp_file_pitched, self.pitch_rate);
                    temp_file_final = temp_file_pitched.clone();
                }
            }
        } else {
            ffmpeg::resample(&temp_file_1, &temp_file_final);
        }

        if self.sox_enabled {
            if !self.ffmpeg_enabled {
                ffmpeg::resample(&temp_file_1, &temp_file_final);
            }

            sox::apply_random_sox_effects(&temp_file_final, &temp_file_final2, 10, &mut random);
        } else {
            temp_file_final2 = temp_file_final
        }

        //Finalize the layer
        ffmpeg::finalize(&temp_file_final2, &output_file_wav, 3);
    }

    fn apply_eq(&mut self, infile: &str, outfile: &str, mut random: &mut SmallRng) {
        let temp_file_1 = format!("{}/temp2.wav", self.path);
        let temp_file_2 = format!("{}/temp3.wav", self.path);
        let mut equalizer = HnwEQ::new();

        //low frequency
        equalizer.apply(infile, &temp_file_1, Freq::Low, &mut random);
        self.eq_details = format!("{}Hz {}db", equalizer.get_frequency(), equalizer.get_gain());
        //medium frequency
        if random.gen_bool(0.5) {
            equalizer.apply(&temp_file_1, &temp_file_2, Freq::Mid, &mut random);
            self.eq_details += &format!(" / {}Hz {}db", equalizer.get_frequency(), equalizer.get_gain());
            //high frequency
            equalizer.apply(&temp_file_2, outfile, Freq::High, &mut random);
        } else {
            equalizer.apply(&temp_file_1, outfile, Freq::Mid, &mut random);
        }
        self.eq_details += &format!(" / {}Hz {}db", equalizer.get_frequency(), equalizer.get_gain());
    }

    pub fn get_eq_details(&self) -> String {
        if self.pitch_chance {
            format!("{} / Pitchrate: {}", self.eq_details, self.pitch_rate)
        } else {
            format!("{}", self.eq_details)
        }
    }
}