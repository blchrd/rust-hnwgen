use crate::effects::ffmpeg;
use rand::{rngs::SmallRng, Rng};

pub enum Freq {Low, Mid, High}

pub struct HnwEQ {
    frequency: i32,
    gain: i32,
    width: i32,
}

impl HnwEQ {
    pub fn new() -> Self {
        Self {
            width: 0,
            gain: 0,
            frequency: 0,
        }
    }

    fn init_eq(&mut self, freq: Freq, mut random: &mut SmallRng) {
        match freq {
            Freq::Low => {
                self.set_random_frequency(250, 500, &mut random);
                self.set_random_gain(5, 11, false, &mut random);
                self.set_random_width(200, 400, &mut random);
            },
            Freq::Mid => {
                self.set_random_frequency(700, 3500, &mut random);
                self.set_random_gain(1, 10, true, &mut random);
                self.set_random_width(100, 300, &mut random);
            },
            Freq::High => {
                self.set_random_frequency(3500, 7000, &mut random);
                self.set_random_gain(1, 10, true, &mut random);
                self.set_random_width(100, 300, &mut random);
            }
        }
    }

    pub fn get_frequency(&mut self) -> i32 {
        self.frequency
    }

    pub fn get_gain(&mut self) -> i32 {
        self.gain
    }

    fn set_random_frequency(&mut self, min: i32, max: i32, random: &mut SmallRng) {
        let temp_min: i32 = min / 100;
        let temp_max: i32 = max / 100;

        self.frequency = random.gen_range(temp_min..=temp_max);
        self.frequency = self.frequency * 100;
    }

    fn set_random_width(&mut self, min: i32, max: i32, random: &mut SmallRng) {
        let temp_min: i32 = min / 100;
        let temp_max: i32 = max / 100;

        self.width = random.gen_range(temp_min..=temp_max);
        self.width = self.width * 100;
    }

    fn set_random_gain(&mut self, min: i32, max: i32, with_negative: bool, random: &mut SmallRng) {
        self.gain = random.gen_range(min..=max);
        if with_negative {
            if random.gen_bool(0.5) {
                self.gain = self.gain * -1;
            }
        }
    }

    pub fn apply (&mut self, infile: &str, outfile: &str, freq: Freq, mut random: &mut SmallRng) {
        self.init_eq(freq, &mut random);
        ffmpeg::apply_eq(infile, outfile, self.frequency, self.width, self.gain)
    }
}