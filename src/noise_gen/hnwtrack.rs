use rand::rngs::SmallRng;

use crate::effects::ffmpeg::mix_layers;
use super::hnwlayer::HnwLayer;

pub struct HnwTrack {
    layers: Vec<HnwLayer>,
    nb_layers: i32,
    path: String,
    title: String,
    length: i32,
    loudness: i16,
    pitch_chance: bool,
    variation_chance: bool,
    ffmpeg_enabled: bool,
    sox_enabled: bool,
}

impl HnwTrack {
    pub fn new(title: String, length: i32, path: String, loudness: i16, nb_layers: i32, pitch_chance: bool, variation_chance: bool, ffmpeg_enabled: bool, sox_enabled: bool) -> Self {
        Self {
            layers: Vec::<HnwLayer>::new(),
            nb_layers,
            path,
            title,
            length,
            loudness,
            pitch_chance,
            variation_chance,
            ffmpeg_enabled,
            sox_enabled,
        }
    }

    pub fn generate_layers(&mut self, mut random: &mut SmallRng) {
        println!("Generate track {}", self.title);
        for i in 0..self.nb_layers {
            let index: usize = i as usize;
            println!("Generate layer {} out of {}", (i+1), (self.nb_layers));
            // /HnwLayer::new("wall-2.wav", "generation/", noiselen, loudness2, true, variation, main_rand.clone())
            //new(outfile: &'a str, path: &'a str, length: i32, loudness: i16, pitch_chance: bool, variation_chance: bool, random: rand::rngs::ThreadRng)
            self.layers.push(HnwLayer::new(
                format!("{}-{}.wav", self.title, (i+1)),
                self.path.clone(),
                self.length,
                self.loudness,
                self.pitch_chance,
                self.variation_chance
            ));
            self.layers[index].ffmpeg_enabled = self.ffmpeg_enabled;
            self.layers[index].sox_enabled = self.sox_enabled;
            self.layers[index].generate(&mut random, None);
        }
    }

    pub fn mix_layers(&mut self) {
        let mut layers_path: Vec<String> = Vec::new();

        for layer in &self.layers {
            layers_path.push(format!("{}/{}", layer.path, layer.outfile));
        }

        mix_layers(layers_path, &self.get_file_path());
    }

    pub fn get_file_path(&mut self) -> String {
        format!("{}/{}.wav", self.path, self.title)
    }

    pub fn get_track_details(&mut self) -> String {
        let mut details: String = self.title.to_string();
        let mut num_layers = 1;

        for layer in &self.layers {
            details += &format!("\r\n  Layer {}: EQ {}", num_layers, layer.get_eq_details());
            num_layers+=1;
        }

        details
    }
}