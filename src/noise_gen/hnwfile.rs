// Rewriting of http://www.jliat.com/HNW/index.html

use std::fs::File;
use std::io::{Write, stdout};
use rand::rngs::SmallRng;
use rand::Rng;

pub struct HnwFile {
    harshness: usize,
    noiselen: i32,
    variation: bool,
    loudness: i16,
}

impl HnwFile {
    pub fn new(harshness: usize, noiselen: i32, loudness: i16, variation: bool) -> Self {
        Self {
            harshness,
            noiselen,
            variation,
            loudness,
        }
    }

    pub fn generate_audio(&mut self, outfile: &str, generator: &mut SmallRng) {
        match File::create(outfile.to_string()) {
            Ok(mut out) => {
                // 16000 16-bit mono signed big endian
                // size of number of samples to smooth
                // lager = more coarse sounds
                out.write_all(&[
                    46, 115, 110, 100,
                    0, 0, 0, 24, // Header size
                    255, 255, 255, 255, // Data size - default ffff
                    0, 0, 0, 3, // sample type 3 = pcm
                    0, 0, 62, 128, // sample rate 16000
                    0 ,0, 0, 2 // 2 channels
                ]).unwrap();

                let mut harshness_temp = self.harshness;
                let harshness_rand = generator.gen_range(1..=3);

                let mut ss = vec![0.0; harshness_temp];
                let mut sa;
                let mut s: i16; // signed two-byte integer for PCM data

                let mut variation = false;

                let mut stdout = stdout();
                print!("Layer generation {}%", "0");
                for z in -1..self.noiselen {
                    let percentage = (z as f32+1.0) * 100.0 / self.noiselen as f32;
                    print!("\rLayer generation {:.1}%", percentage);
                    stdout.flush().unwrap();

                    if self.variation {
                        if generator.gen::<u32>() % 1500 == 0 {
                            if !variation {
                                print!("\rLayer generation {:.1}% {}", percentage, format!("Variation started at {:.1}%    ", percentage));
                                variation = true;

                                harshness_temp = generator.gen_range(80..=2580);
                            } else {
                                print!("\rLayer generation {:.1}% {}", percentage, format!("Variation ended at {:.1}%    ", percentage));
                                variation = false;

                                harshness_temp = self.harshness;
                            }
                            ss = vec![0.0; harshness_temp];
                        }
                    }

                    if self.noiselen < 1 {
                        if z == -1 {
                            continue;
                        } else {
                            break;
                        }
                    }

                    for _ in 0..500 {
                        let r = generator.gen::<i16>();
                        s = r;
                        
                        if harshness_temp > 0 {
                            ss.rotate_left(1);
                            ss[harshness_temp - 1] = s as f64;
                        }
                        
                        sa = ss.iter().sum::<f64>();
                        
                        let harshness_div = harshness_temp / generator.gen_range(1..=harshness_rand);
                        let harshness_div = if harshness_div == 0 { 1 } else { harshness_div };
                        
                        sa /= harshness_div as f64;
                        s = sa as i16;

                        if s > 0 && s < self.loudness {
                            s += self.loudness;
                        }
                        if s < 0 && s > -self.loudness {
                            s -= self.loudness;
                        }

                        let b1 = (s & 0xFF) as u8;
                        let b2 = ((s >> 8) & 0xFF) as u8;

                        out.write_all(&[b2, b1]).unwrap();
                    }
                }

                println!("\rLayer generation done                                      ");
            }
            Err(e) => {
                eprintln!("Error: {}", e);
                std::process::exit(1);
            }
        }
    }
}