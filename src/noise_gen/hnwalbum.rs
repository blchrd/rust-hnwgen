use crate::image_gen::hnwimage::HnwImage;
use crate::name_gen::hnwstring::{self};
use crate::Config;
use super::hnwtrack::HnwTrack;
use crate::effects::{ffmpeg, sox};
use rand::rngs::SmallRng;
use rust_search::SearchBuilder;
use std::cmp;

use rand::Rng;
use std::fs;
use id3::{Tag, TagLike, Version};
use chrono::Datelike;
pub struct HnwAlbum {
    length: i32,
    nb_tracks: i32,
    nb_layers: i32,
    pitch_chance: bool,
    variation_chance: bool,
    taint_cover: bool,
    l_system: bool,
    tribute_path: String,
    with_tribute_cover: bool,
    tribute_title: String,
    ffmpeg_enabled: bool,
    sox_enabled: bool,
    post_effect: bool,
    seed: u64,
    config: Config,
}

impl HnwAlbum {
    pub fn new(
        length: i32,
        nb_tracks: i32,
        nb_layers: i32,
        pitch_chance: bool,
        variation_chance: bool,
        taint_cover: bool,
        l_system: bool,
        tribute_path: String,
        with_tribute_cover: bool,
        tribute_title: String,
        ffmpeg_enabled: bool,
        sox_enabled: bool,
        post_effect: bool,
        seed: u64,
        config: Config,
    ) -> Self {
        Self {
            length,
            nb_tracks,
            nb_layers,
            pitch_chance,
            variation_chance,
            taint_cover,
            l_system,
            tribute_path,
            with_tribute_cover,
            tribute_title,
            ffmpeg_enabled,
            sox_enabled,
            post_effect,
            seed,
            config,
        }
    }

    pub fn generate_hnw_album(&mut self, mut random: &mut SmallRng) {
        if self.tribute_path != "".to_string() {
            self.generate_tribute_hnw_album(self.tribute_path.clone(), &mut random);
            return;
        }

        //variable
        let loudness: i16 = random.gen_range(600..=3100);
        let mut info_text: String;
        let mut generated_strings = hnwstring::GeneratedStrings::default();

        //project name
        generated_strings.project_name = hnwstring::get_string(
            hnwstring::Type::ProjectName,
            generated_strings.clone(),
            0,
            &mut random,
            &self.config,
        );

        //title of album
        generated_strings.album_title = hnwstring::get_string(
            hnwstring::Type::AlbumTitle,
            generated_strings.clone(),
            0,
            &mut random,
            &self.config,
        );

        //create directory
        let mut dir_name_temp: String = format!("generation/{}", generated_strings.clone().album_title.unwrap_or("untitled".to_string()));
        let mut number = 1;
        let mut metadata = fs::metadata(dir_name_temp.clone()).ok();
        if metadata.is_some() {
            while metadata.unwrap().is_dir() {
                dir_name_temp = format!("generation/{}-{}", generated_strings.clone().album_title.unwrap_or("untitled".to_string()), number);
                metadata = fs::metadata(dir_name_temp.clone()).ok();
                number += 1;
            }
        }
        let dir_name = dir_name_temp;
        fs::create_dir_all(dir_name.clone()).ok();

        //create working directory
        let work_dir_name: String = format!("{}/temp", dir_name);
        fs::create_dir_all(work_dir_name.clone()).ok();

        //generate the cover
        let mut cover_image = HnwImage::new(1400, 1400);
        cover_image.generate_image(
            &format!("{}/cover.jpg", dir_name),
            &hnwstring::get_string(
                hnwstring::Type::CoverTitle, 
                generated_strings.clone(), 
                0,
                &mut random,
                &self.config,
            ).unwrap_or("Untitled".to_string()),
            self.taint_cover,
            self.l_system,
            &mut random
        );

        //debug output
        println!("Project name: {}", generated_strings.clone().project_name.unwrap_or("unknown".to_string()));
        println!("Album: {}", generated_strings.clone().album_title.unwrap_or("untitled".to_string()));
        println!("Number of track: {}", self.nb_tracks.to_string());
        println!("Total length: {} minute(s)", self.length.to_string());
        println!("Loudness: {}", loudness.to_string());

        //text details
        info_text = format!("Project name: {}\r\n", generated_strings.clone().project_name.unwrap_or("unknown".to_string()));
        info_text += &format!("Album: {}\r\n", generated_strings.clone().album_title.unwrap_or("untitled".to_string()));
        info_text += &format!("Number of track {}\r\n", self.nb_tracks);
        info_text += &format!("Seed: {}\r\n", self.seed);
        info_text += &format!("Total length: {} minute(s)\r\n", self.length);
        if self.l_system {
            info_text += &format!("L-System for cover: {}\r\n", cover_image.get_lsystem_json());
        }

        //generate the tracks
        // 3840 = 1m
        let track_length = (3840 * self.length) / self.nb_tracks;
        for num_track in 1..=self.nb_tracks {
            let track_title = hnwstring::get_string(
                hnwstring::Type::TrackTitle, 
                generated_strings.clone(),
                num_track as u32,
                &mut random, 
                &self.config
            ).unwrap_or("Untitled".to_string());
            self.config = confy::load("hnw", None).ok().unwrap();
            let track_file_name: String = format!("{} - {}", num_track, track_title.clone());

            let mut hnw_track: HnwTrack = self.generate_hnw_track(
                track_file_name.clone(),
                track_length,
                loudness,
                work_dir_name.clone(),
                format!("{}/{}.wav",dir_name,track_file_name),
                true,
                &mut random,
            );

            info_text += &(format!("\r\n{}", hnw_track.get_track_details()));

            //Write ID3 tags to the wav file
            //https://docs.rs/id3/latest/id3/
            let current_date = chrono::Utc::now();
            let mut tags = Tag::new();
            tags.set_year(current_date.year());
            tags.set_track(num_track as u32);
            tags.set_total_tracks(self.nb_tracks as u32);
            tags.set_artist(generated_strings.clone().project_name.unwrap_or("unknown".to_string()));
            tags.set_album(generated_strings.clone().album_title.unwrap_or("untitled".to_string()));
            tags.set_title(track_title.clone());
            tags.set_genre("Harsh Noise Wall");
            let _ = tags.write_to_path(format!("{}/{}.wav",dir_name,track_file_name), Version::Id3v24);
        }

        //write details in a .txt file
        fs::write(format!("{}/details.txt",dir_name.clone()), info_text).ok();        

        //clean the working directory
        fs::remove_dir_all(work_dir_name.clone()).ok();
    }

    fn generate_tribute_hnw_album(&mut self, source_path: String, mut random: &mut SmallRng) {
        // Create directories
        let work_dir_name = format!("{}/work", source_path);
        let final_directory = format!("{}/final", source_path);
        if fs::metadata(work_dir_name.clone()).ok().is_some() {
            fs::remove_dir_all(work_dir_name.clone()).ok();
        }
        fs::create_dir_all(work_dir_name.clone()).ok();
        if fs::metadata(final_directory.clone()).ok().is_some() {
            fs::remove_dir_all(final_directory.clone()).ok();
        }
        fs::create_dir_all(final_directory.clone()).ok();

        // Generate the cover
        if self.with_tribute_cover {
            let mut hnw_image = HnwImage::new(1400, 1400);
            hnw_image.generate_tribute_cover(
                &format!("{}/cover.jpg", source_path),
                &format!("{}/cover.jpg", final_directory),
                &self.tribute_title,
                &mut random
            );
        }

        // Parse the source path for wav
        let search: Vec<String> = SearchBuilder::default()
            .location(source_path)
            .ext("wav")
            .depth(1)
            .ignore_case()
            .build()
            .collect();

        let mut index = 0;
        for path in search {
            println!("{}", path);
            let original_audio = path.clone();
            let track_name = path.clone().split("/").last().unwrap().to_string();
            let wall_track_path = format!("{}/{}-wall-{}.wav", work_dir_name.clone(), track_name, index);

            // generate wall
            self.generate_hnw_track(
                format!("wall-temp-{}", index),
                3840 * 10, 
                random.gen_range(600..=3100) as i16, 
                work_dir_name.clone(), 
                wall_track_path.clone(),
                false,
                &mut random,
            );

            // mix original audio and wall
            let track_path = self.generate_hnw_tribute_track(
                original_audio, 
                wall_track_path.clone(), 
                work_dir_name.clone(), 
                index,
                60,
                600
            );

            // copy the file
            fs::copy(track_path.clone(), format!("{}/{}", final_directory, track_name)).ok();

            //Clean working directory
            self.clean_directory(work_dir_name.clone());
            
            index += 1;
        }

        // Clean the working directory
        fs::remove_dir_all(work_dir_name.clone()).ok();
    }

    fn generate_hnw_tribute_track(&mut self, original_audio_path: String, wall_audio_path: String, work_directory: String, index: i32, original_audio_duration_in_second: i32, track_duration_in_second: i32) -> String {
        let time = cmp::max(original_audio_duration_in_second, 20);
        let track_length = cmp::max(track_duration_in_second, 300);

        // Make silent audio to mix with original audio file
        let silent_audio = format!("{}/silence.wav", work_directory);
        ffmpeg::generate_silent_track(&silent_audio.clone(), track_length);

        // Paths to the temporary audio file
        let cut_audio = format!("{}/cut-{}.wav", work_directory, index);
        let cut_audio_fade_out = format!("{}/cut-{}-fo.wav", work_directory, index);
        let cut_audio_final = format!("{}/cut-{}-fi.wav", work_directory, index);
        let wall_audio_fade_in = format!("{}/wall-{}-fi.wav", work_directory, index);
        let wall_audio_final_volume = format!("{}/wall-{}-fv.wav", work_directory, index);
        let mixed_audio = format!("{}/mix-{}.wav", work_directory, index);
        let final_audio_before_fade_out = format!("{}/final-bfo-{}.wav", work_directory, index);
        let final_audio = format!("{}/final-{}.wav", work_directory, index);
        
        // cut the end of the original audio + fade out
        ffmpeg::cut(&original_audio_path, &cut_audio, 0, time);
        ffmpeg::fade_out(&cut_audio, &cut_audio_fade_out, time / 3, (time / 3) * 2);
        // mix silent and cut audio
        ffmpeg::mix_layers(vec![silent_audio.clone(), cut_audio_fade_out.clone()], &cut_audio_final);
        // fade in the wall and turn the volume to 0 for the first seconds
        ffmpeg::fade_in(&wall_audio_path, &wall_audio_fade_in, time, 10);
        ffmpeg::cut_volume(&wall_audio_fade_in, &wall_audio_final_volume, 0, 10);
        // mix wall and cutted original audio + fade out the final result
        ffmpeg::mix_layers(vec![cut_audio_final.clone(), wall_audio_final_volume.clone()], &mixed_audio);
        ffmpeg::finalize(&mixed_audio, &final_audio_before_fade_out, 3);
        ffmpeg::fade_out(&final_audio_before_fade_out, &final_audio, track_length / 20, track_length - 30);

        final_audio
    }

    fn generate_hnw_track(&mut self, title: String, track_length: i32, loudness: i16, work_directory: String, final_path: String, clean_directory: bool, mut random: &mut SmallRng) -> HnwTrack {
        let mut hnw_track = HnwTrack::new(
            title,
            track_length,
            work_directory.clone(),
            loudness,
            self.nb_layers,
            self.pitch_chance,
            self.variation_chance,
            self.ffmpeg_enabled,
            self.sox_enabled,
        );
        hnw_track.generate_layers(&mut random);
        hnw_track.mix_layers();

        let mut hnw_final_path = hnw_track.get_file_path();
        if self.post_effect {
            // Apply effect on the final mix
            hnw_final_path = format!("{}/{}", work_directory, "final_post.wav");
            sox::apply_random_sox_effects(&hnw_track.get_file_path(), &hnw_final_path, 4, &mut random);
        }

        if self.nb_layers == 1 {
            // If there is only one layer, just copy the file in the final directory
            fs::copy(hnw_final_path, final_path.clone()).ok();
        } else {
            // The layering lower the volume of the track, therefore we need to adapt the final volume
            ffmpeg::finalize(&hnw_final_path, &final_path.clone(), 3);
        }

        //Clean working directory
        if clean_directory {
            self.clean_directory(work_directory.clone());
        }

        hnw_track
    }

    fn clean_directory(&self, dir_path: String) {
        let dir_fs = fs::read_dir(dir_path.clone());
        if let Ok(dir) = dir_fs {
            for entry in dir {
                if let Ok(entry) = entry {
                    let path = entry.path();
    
                    if path.is_dir() {
                        fs::remove_dir_all(path).expect("Failed to remove a dir");
                    } else {
                        fs::remove_file(path).expect("Failed to remove a file");
                    }
                };
            }
        };
    }
}
